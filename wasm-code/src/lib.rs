mod utils;
//use std::{thread, time};


use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
    alert("Hello, {{project-name}}!");
}

#[wasm_bindgen]
pub fn add_one(n: u32) -> u32 {
    // let sleep_duration = time::Duration::from_millis(10000);

    // thread::sleep(sleep_duration);
    n + 1
}
