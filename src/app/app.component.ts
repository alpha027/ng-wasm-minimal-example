import { Component } from '@angular/core';
import { add_one } from 'wasm-minimal-example-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'wasm-minimal-example';
  result!:number;
  workerResult!:number;
  private webWorker!: Worker;
  private output!: any;

  ngOnInit() {
    if(typeof Worker !== 'undefined') {
        this.webWorker = new Worker('./my-worker.worker', { type: 'module' });
        this.webWorker.onmessage = ({ data }) => {
            console.log(`on Init page got message from worker: ${data}`);
            this.output = data
        }
    }
  }

  add_one_wasm(){
    var random_number = Math.floor(Math.random() * 100);
    console.log("random_number: ", random_number);
    this.result = add_one(random_number);
  }

  public async add_one_worker(){

    if (typeof Worker !== 'undefined') {
      // Create a new
      const worker = new Worker(new URL('./app.worker', import.meta.url));
      worker.onmessage = ({ data }) => {
        console.log(`page got message: ${data}`);
      };
      var random_number = Math.floor(Math.random() * 100);
      console.log("random_number: ", random_number);
      worker.postMessage({ number: random_number });
    } else {
    }
  }
}

