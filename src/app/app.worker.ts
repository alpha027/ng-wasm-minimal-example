/// <reference lib="webworker" />
import { add_one } from 'wasm-minimal-example-lib';

function normal_add_one(number: number):number{
  return number + 1;
}

addEventListener('message', ({ data }) => {
  // What I want to be able to do is the line below but it doesn't work
  // const response = add_one(data.number);
  const response = normal_add_one(data.number);
  postMessage(response);
});
